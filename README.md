# Descriptif du projet

- Auteur : Florian MUNIER
- Contact : florian.munier@imt-atlantique.net
- Formation : MS Infrastructures Cloud et DevOps
- Date : 31/01/2023 - 27/02/2023
- Description : Ce projet a pour but de déployer l'application Vapormap sur une instance d'une infrastructure générée via le projet terraform
- projet_terraform : https://gitlab.imt-atlantique.fr/f21munie/projet_terraform.git

> A noter qu'il faut au préalable avoir créé l'infrastructure via Terraform (lien du GitLab ci-dessus).

# Clone du projet

```sh
git clone https://gitlab.imt-atlantique.fr/f21munie/projet_terraform.git
```

# Création d'un environnement virtuel

```sh
cd projet_ansible_vapormap
python3 -m venv ./venv/ansible
source ./venv/ansible/bin/activate
pip install ansible
deactivate
```
# Création des fichiers hosts.ini, .ssh et ssh.cfg

Le projet terraform a généré les fichiers "hosts.ini", ".ssh" et "ssh.cfg".
Copiez les dans le répertoire de travail.
Ajouter un chmod 600 aux clés.

```sh
cp ../projet_terraform/hosts.ini projet_ansible_vapormap/
cp ../projet_terraform/.ssh projet_ansible_vapormap/
cp ../projet_terraform/ssh.cfg projet_ansible_vapormap/
chmod 600 .ssh/*
```
# Vérifier que les instances sont joignables

```sh
ANSIBLE_HOST_KEY_CHECKING=False ansible -i hosts.ini all -m ping
```

# Déploiement de l'application Vapormap

Dans le playbook, vous pouvez modifier <hosts> pour choisir l'instance sur laquelle vous voulez déployer le playbook de déploiement. Par exemple hosts: node01.
Par défaut, hosts est prédéfinit sur bastion.

> Si vous choisissez de déployer le playbook sur node01, il vous faudra modifier le fichier "hosts.ini" en ajoutant à la fin de la ligne correspondante à node01 : PUB_API_IP=<ip_publique_node01> qui est l'IP public de node01 dans le fichier "public_ip.txt".
node01 ansible_host=192.168.1.183  PUB_API_IP=10.29.245.115

```sh
source ./venv/ansible/bin/activate
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts.ini deploy.yml
deactivate
```

# Accéder à l'application

## Déploiement avec docker-compose

- Frontend

```sh
http://<PUB_API_IP>:8081
```

- API

```sh
http://<PUB_API_IP>:8082/api/points/
```

## Déploiement avec Kubernetes

- Frontend

```sh
http://<PUB_API_IP>
```

- API

```sh
http://<PUB_API_IP>/api/points/
```

# Suppression de l'application Vapormap

Dans le playbook, vous pouvez modifier <hosts> pour choisir l'instance sur laquelle vous voulez déployer le playbook de destruction. Par exemple hosts: node01.
Par défaut, hosts est prédéfinit sur bastion.

```sh
source ./venv/ansible/bin/activate
ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i hosts.ini destroy.yml
deactivate
```
